package com.lpro4.androidapp

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.GeoPoint

data class Area(val name: String? = "",
                val points: ArrayList<GeoPoint>? = arrayListOf()) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            readGeoPointArrayList(parcel)) {
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        writeGeoPointList(points, parcel)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Area> {
        override fun createFromParcel(parcel: Parcel): Area {
            return Area(parcel)
        }

        override fun newArray(size: Int): Array<Area?> {
            return arrayOfNulls(size)
        }
    }
}