package com.lpro4.androidapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_home.*
import com.lpro4.androidapp.AlertsActivity
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : BaseActivity() {

    val REQUEST_PHONE_CALL = 1
    private lateinit var device: Device
    private lateinit var alertsList: ArrayList<*>

    private val SETTINGS_REQUEST_CODE = 5555


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setContentView(R.layout.activity_home)

        // Setup

        val bundle: Bundle? = intent.extras
        val email: String? = bundle?.getString("email")
        device = bundle?.get("device") as Device
        alertsList = device.alerts
        //alertsList = bundle?.get("alerts") as ArrayList<*>
        setup(email ?: "")


        // Guardado de datos

        val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        prefs.putString("email", email)
        prefs.apply()

    }

    private fun setup(email: String) {

        title = getString(R.string.home)
        emailTextView.text = email
        deviceTextView.text = device.name

        logOutButton.setOnClickListener {

            // Borrado de datos

            val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            prefs.clear()
            prefs.apply()

            FirebaseAuth.getInstance().signOut()

            //onBackPressed()
            val intent = Intent(this, MainActivity::class.java).apply {
            }
            startActivity(intent)
        }

        settingsButton.setOnClickListener {

            val settingsIntent = Intent(this, SettingsActivity::class.java).apply {
                putExtra("email", email)
                putExtra("device-id", device.id)
                putExtra("device", device)
            }

            startActivityForResult(settingsIntent,SETTINGS_REQUEST_CODE)
        }

        alertsButton.setOnClickListener {

            val alertsIntent = Intent(this, AlertsActivity::class.java).apply {
                putExtra("alerts", alertsList as ArrayList<Alert>)
                putExtra("device", device)
                putExtra("device-id", device.id)
            }

            startActivity(alertsIntent)
        }

        callButton.setOnClickListener {
            startCall(device.phoneNumber)
        }

        locationButton.setOnClickListener {
            val locationIntent = Intent(this, LocationActivity::class.java).apply {
                putExtra("device", device)
            }

            startActivity(locationIntent)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SETTINGS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //val geoPoints: ArrayList<GeoPoint> = data?.extras?.get("geoPoints") as ArrayList<GeoPoint>
            //device.geoPoints = geoPoints

            Firebase.firestore.collection("Devices").document(device.id!!).get().addOnSuccessListener { document->
                device = document.toObject<Device>()!!
                deviceTextView.text = device.name
            }

            deviceTextView.text = device.name
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun startCall(phoneNumber: String?) {
        val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))

        startActivity(callIntent)
    }

}