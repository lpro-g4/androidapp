package com.lpro4.androidapp

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint

data class Device(
        var id: String? = "",
        var name: String? = "",
        val phoneNumber: String? = "",
        var alerts: ArrayList<Alert> = arrayListOf(),
        var contactNumber: String? = "",
        var points: ArrayList<GeoPoint>? = arrayListOf(),
        var inactivityAlert: Boolean? = false,
        var areas: ArrayList<Area> = arrayListOf(),
        var selectedArea: String? = "",
        var latestPosition: GeoPoint? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readArrayList(Alert::class.java.classLoader) as ArrayList<Alert>,
            parcel.readString(),
            //parcel.readArrayList(GeoPoint::class.java.classLoader) as ArrayList<GeoPoint>,
            readGeoPointArrayList(parcel),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readArrayList(Area::class.java.classLoader) as ArrayList<Area>,
            parcel.readString(),
            GeoPoint(parcel.readDouble(), parcel.readDouble())
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(phoneNumber)
        parcel.writeList(alerts)
        parcel.writeString(contactNumber)
        writeGeoPointList(points, parcel)
        parcel.writeValue(inactivityAlert)
        parcel.writeList(areas)
        parcel.writeString(selectedArea)
        parcel.writeDouble(latestPosition?.latitude!!)
        parcel.writeDouble(latestPosition?.longitude!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Device> {
        override fun createFromParcel(parcel: Parcel): Device {
            return Device(parcel)
        }

        override fun newArray(size: Int): Array<Device?> {
            return arrayOfNulls(size)
        }
    }
}