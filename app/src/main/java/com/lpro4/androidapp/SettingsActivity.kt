package com.lpro4.androidapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.common.PlaceConstants
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_settings.view.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class SettingsActivity : BaseActivity() {

    private var mapView: MapView? = null
    private val db = Firebase.firestore
    private var contactNumber: String = ""
    private var deviceName: String = ""
    private var inactivityAlert: Boolean = false
    private var pointsArray: Array<Point> = arrayOf()
    private lateinit var device: Device


    companion object {
        private val PLACE_SELECTION_REQUEST_CODE = 56789
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = getString(R.string.settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val bundle: Bundle? = intent.extras
        val email: String? = bundle?.getString("email")
        val deviceId: String? = bundle?.getString("device-id")
        device = bundle?.get("device") as Device


        // MapBox

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

        setContentView(R.layout.activity_settings)

        editTextPhone.setText(device.contactNumber)
        editTextName.setText(device.name)
        swInactivity.isChecked = device.inactivityAlert!!

        mapView = findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)

        initializeMap(device.areas.find { it.name == device.selectedArea }?.points?.let {
            polygonFromGeoPoints(
                it
            )
        })
        //getDeviceConfig(deviceId ?: "")

        // dropDown menu
        val items = device.areas.map { it.name }
        val adapter = ArrayAdapter(this, R.layout.area_item, items)
        //val adapter = AreaAdapter(device.areas,device,this, deviceId!!)
        (menu.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        actArea.setText(device.selectedArea, false)
//        actArea.setOnLongClickListener {
//            println("ad")
//            true
//        }
        actArea.setOnItemClickListener { parent, view, position, id ->

            val selectedArea = device.areas.find { it.name == actArea.text.toString() }
            val polygon = polygonFromGeoPoints(selectedArea?.points!!)
            initializeMap(polygon)
        }


        areaButton.setOnClickListener {

//            val intent = PlacePicker.IntentBuilder()
//                    .accessToken(Mapbox.getAccessToken()!!)
//                    .placeOptions(
//                            PlacePickerOptions.builder()
//                                    .includeReverseGeocode(false)
//                                    .statingCameraPosition(
//                                            CameraPosition.Builder()
//                                                    .target(LatLng(42.2376602, -8.7247205))
//                                                    .zoom(16.0)
//                                                    .build())
//                                    .build())
//                    .build(this)
            val intent = Intent(this, AreaActivity::class.java).apply { }

            startActivityForResult(intent, PLACE_SELECTION_REQUEST_CODE)
        }

        sendButton.setOnClickListener {
            updateDeviceConfig2(deviceId ?: "")
        }

    }

    private fun getDeviceConfig(deviceId: String) {


        val docRef = db.collection("Devices").document(deviceId)

        docRef.get().addOnSuccessListener { document ->
            if (document != null && document.exists()) {

                contactNumber = document.get("contactNumber") as String
                editTextPhone.setText(contactNumber)

                deviceName = document.get("name") as String
                editTextName.setText(deviceName)

                inactivityAlert = document.get("inactivityAlert") as Boolean
                swInactivity.isChecked = inactivityAlert


                var points = document.get("points")
                if (points != null) {
                    points = points as ArrayList<GeoPoint>
                    if (points.size != 0) {
                        val listOfPoints: MutableList<Point> = mutableListOf()
                        for (p in points) {
                            val point = Point.fromLngLat(p.longitude, p.latitude)
                            listOfPoints.add(point)
                            pointsArray = listOfPoints.toTypedArray()
                        }
                        val polygon = Polygon.fromLngLats(listOf(listOfPoints))
                        initializeMap(polygon)
                    }
                } else {
                    initializeMap(null)
                }

            }

        }
            .addOnFailureListener {
                println("ERROR: $it")
            }
    }

    private fun initializeMap(polygon: Polygon?) {

        mapView?.getMapAsync { mapboxMap ->

            mapboxMap.setStyle(Style.MAPBOX_STREETS) {

// Map is set up and the style has loaded. Now you can add data or make other map adjustments

//                val symbolManager = SymbolManager(mapView!!, mapboxMap, it)
//                symbolManager.iconAllowOverlap = true
//
//                val symbol: Symbol = symbolManager.create(SymbolOptions()
//                        .withLatLng(LatLng(60.169091, 24.939876))
//                        .withIconImage(R.dcrawable.map_default_map_marker.toString())
//                        .withIconSize(2.0f))

                if (polygon != null) {
                    val source = GeoJsonSource("source-id", polygon)
                    it.addSource(source)
                    it.addLayer(
                        FillLayer(
                            "area-layer",
                            "source-id"
                        ).withProperties(
                            PropertyFactory.fillColor("#00e9ff"),
                            PropertyFactory.fillOpacity(0.5f)
                        )
                    )

                    val firstPoint: Point = polygon.coordinates()[0][0]
                    val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(firstPoint.latitude(), firstPoint.longitude()))
                        .zoom(14.0)
                        .build()
                    mapboxMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(cameraPosition),
                        100
                    )
                }

            }

        }
    }

    private fun updateDeviceConfig2(deviceId: String) {

        device.selectedArea = actArea.text.toString()
        device.points = device.areas.find { it.name.equals(device.selectedArea) }?.points
        device.inactivityAlert = swInactivity.isChecked
        device.name = editTextName.text.toString()
        device.contactNumber = editTextPhone.text.toString()

        db.collection("Devices").document(deviceId)
            .set(device, SetOptions.merge())
            .addOnSuccessListener {
                Timber.d("DocumentSnapshot successfully written!")
                val returningIntent = Intent()
                //returningIntent.putExtra("geoPoints",arrayOfGeoPoint)
                setResult(RESULT_OK, returningIntent)
                finish()
            }
            .addOnFailureListener { e -> Timber.tag("Settings").w(e, "Error writing document") }
    }

    private fun updateDeviceConfig(deviceId: String) {

        val arrayOfGeoPoint: ArrayList<GeoPoint> = arrayListOf()
        for (p in pointsArray) {
            arrayOfGeoPoint.add(GeoPoint(p.latitude(), p.longitude()))
        }

        val selectedAreaName: String = actArea.text.toString()
        val selectedArea: Area? = device.areas.find { it.name.equals(selectedAreaName) }
        val aPoints = selectedArea?.points

        val devicex = hashMapOf(
            "name" to editTextName.text.toString(),
            "contactNumber" to editTextPhone.text.toString(),
            "points" to arrayOfGeoPoint,
            "inactivityAlert" to swInactivity.isChecked,
            "areas" to device.areas,
            "alerts" to device.alerts
        )

        db.collection("Devices").document(deviceId)
            .set(devicex, SetOptions.merge())
            .addOnSuccessListener {
                Timber.d("DocumentSnapshot successfully written!")
                val returningIntent = Intent()
                //returningIntent.putExtra("geoPoints",arrayOfGeoPoint)
                setResult(RESULT_OK, returningIntent)
                finish()
            }
            .addOnFailureListener { e -> Timber.tag("Settings").w(e, "Error writing document") }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 56789 && resultCode == Activity.RESULT_OK) {

            val areaName: String? = data?.extras?.getString("area-name")
            val points = pointsToGeoPoints(data?.extras?.get("points") as Array<Point>)
            val newArea = Area(areaName, points)
            device.areas.add(newArea)
            device.points = points
            actArea.setText(areaName, false)

            val polygon = polygonFromGeoPoints(points)
            pointsArray = data.extras?.get("points") as Array<Point>


//            val lat = carmenFeature?.center()?.latitude()
//            val long = carmenFeature?.center()?.longitude()
            val camera = data.extras!!.get(PlaceConstants.MAP_CAMERA_POSITION) as CameraPosition

            mapView?.getMapAsync { mapboxMap ->

                mapboxMap.setStyle(Style.MAPBOX_STREETS) {


                    val source = GeoJsonSource("source-id", polygon)
                    it.addSource(source)
                    it.addLayer(
                        FillLayer(
                            "area-layer",
                            "source-id"
                        ).withProperties(
                            PropertyFactory.fillColor("#00e9ff"),
                            PropertyFactory.fillOpacity(0.5f)
                        )
                    )

// Map is set up and the style has loaded. Now you can add data or make other map adjustments

                }

                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera), 100)

            }


        }
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

//    override fun onSaveInstanceState(outState: Bundle?) {
//        super.onSaveInstanceState(outState)
//        mapView?.onSaveInstanceState(outState)
//    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

}