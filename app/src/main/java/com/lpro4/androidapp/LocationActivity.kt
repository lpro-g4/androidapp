package com.lpro4.androidapp

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import kotlinx.android.synthetic.main.activity_alerts.*
import kotlinx.android.synthetic.main.activity_settings.*

class LocationActivity : BaseActivity() {
    lateinit var device: Device
    lateinit var symbolManager: SymbolManager
    var initialization: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.location)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val bundle: Bundle? = intent.extras
        device = bundle?.get("device") as Device

        Mapbox.getInstance(
            this,
            "pk.eyJ1IjoibHBybzQyMDIxIiwiYSI6ImNrbHkxMTd4ajE1d2QycHBsaXZ3M2FpeXUifQ.sEZWbH6nifNb_-TqjXDXNw"
        )
        setContentView(R.layout.activity_location)

        //mapView = findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        refresh()
    }


    private fun refresh() {
        val db = Firebase.firestore
        db.collection("Devices").document(device.id!!).get().addOnSuccessListener {
            device = it.toObject()!!
            device.alerts.sort()
            mapView?.getMapAsync { mapboxMap ->

                mapboxMap.setStyle(Style.MAPBOX_STREETS) {

// Map is set up and the style has loaded. Now you can add data or make other map adjustments

                    if (initialization) {
                        symbolManager = SymbolManager(mapView!!, mapboxMap, it)
                        initialization = false
                    }
                    symbolManager.iconAllowOverlap = true

                    val cameraPosition = CameraPosition.Builder()
                        .target(
                            LatLng(
                                device.latestPosition?.latitude!!,
                                device.latestPosition!!.longitude
                            )
                        )
                        .zoom(14.0)
                        .build()
                    mapboxMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(cameraPosition),
                        100
                    )
                    it.addImage(
                        "myMarker",
                        BitmapFactory.decodeResource(
                            baseContext.resources,
                            R.drawable.map_default_map_marker
                        )
                    )
                    val target: LatLng =
                        LatLng(device.latestPosition!!.latitude, device.latestPosition!!.longitude)
                    symbolManager.deleteAll()
                    symbolManager.create(
                        SymbolOptions()
                            .withLatLng(target)
                            .withIconImage("myMarker")
                    )

                }

            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_with_refresh, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.item_refresh -> {
                refresh()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}