package com.lpro4.androidapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_devices.*
import kotlinx.android.synthetic.main.item_text_view.view.*
import kotlinx.android.synthetic.main.item_text_view.view.tvName
import kotlinx.android.synthetic.main.item_text_view2.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList


class DevicesActivity : BaseActivity() {

    private val db = Firebase.firestore

    private lateinit var user: User
    private var devicesNames: Array<String> = arrayOf()
    private var devicesIds: Array<String> = arrayOf()
    private lateinit var email: String

//    private val mAdapter by lazy {
//        MyAdapter(myDataSet)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devices)

        title = getString(R.string.devices)

        val bundle: Bundle? = intent.extras
        email = bundle?.getString("email") ?: ""
        notification(email)

        // Guardado de datos

        val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        prefs.putString("email", email)
        prefs.apply()

        getUserData(email)

        val layoutManager = LinearLayoutManager(baseContext)

        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))

        //recyclerView.adapter = MyAdapter(myDataSet) { position -> showHome(position) }
//        {
//            Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show()
//        }

        logOutButton.setOnClickListener {

            // Borrado de datos

            val prefs = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
            prefs.clear()
            prefs.apply()

            FirebaseAuth.getInstance().signOut()

            //onBackPressed()
            val intent = Intent(this, MainActivity::class.java).apply {
            }
            startActivity(intent)
        }
    }

//    private fun showHome(position: Int) {
//        val homeIntent = Intent(this, HomeActivity::class.java).apply {
//            putExtra("email", email)
//            putExtra("device", devicesNames[position])
//            putExtra("device-id", devicesIds[position])
//            //putExtra("device-number",) TODO
//        }
//        startActivity(homeIntent)
//        //Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show()
//    }
//
//    private fun showHome(user: User, device: Device) {
//        val homeIntent = Intent(this, HomeActivity::class.java).apply {
//            putExtra("email", user.email)
//            putExtra("device", device.name)
//            putExtra("device-id", device.id)
//            putExtra("device-number",device.number)
//        }
//        startActivity(homeIntent)
//        //Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show()
//    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.settings_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.item_spanish -> {
                println("click spanish")
                BaseActivity.dLocale = Locale("es")
                resources.configuration.setLocale(Locale("es"))
                recreate()
            }
            R.id.item_english -> {
                println("click english")
                BaseActivity.dLocale = Locale("en")
                resources.configuration.setLocale(Locale("en"))
                recreate()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun notification(email: String) {

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                println("Fetching FCM registration token failed")
                return@OnCompleteListener
            }
            val token = task.result
            MyFirebaseMessagingService.updateToken(token.toString(), email)
        })

    }

    private fun showHome(user: User, position: Int) {
        val device = user.devices[position]
        val homeIntent = Intent(this, HomeActivity::class.java).apply {
            putExtra("email", user.email)
            putExtra("device", device)
        }
        startActivity(homeIntent)
        //Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show()
    }

    private fun getUserData(email: String) {

        user = User(email)

        val docRef = db.collection("Users").document(email)

        docRef.get().addOnSuccessListener { document ->
            if (document != null && document.exists()) {
                var devices = document.get("devices")
                if (devices != null) {
                    devices = devices as ArrayList<String>
                    for (d in devices) {
                        // println(getDeviceFromDocument(document))
//                        Firebase.firestore.collection("Devices").document(d.id).collection("alerts").get().addOnSuccessListener {
//                            println("asd")
//                            for (doc in it) {
//                                doc.get("type").toString()
//                            }
//                        }
//                                .addOnFailureListener {
//                                    println("ERROR $it")
//                                }

                        GlobalScope.launch(Dispatchers.IO) {
                            val device: Device? = db.collection("Devices").document(d).get().await().toObject<Device>()
                          //  val device: Device? = d.get().await().toObject<Device>()
                           // val alerts = Firebase.firestore.collection("Devices").document(d.id).collection("alerts").get().await().toObjects<Alert>()
                            //val areas = Firebase.firestore.collection("Devices").document(d.id).collection("areas").get().await().toObjects<Area>()
                            //device?.alerts = alerts as ArrayList<Alert>
                            //device?.areas = areas as ArrayList<Area>
                            if (device != null) {
                                Collections.sort(device.alerts)
                                Collections.sort(device.alerts,)
                            }
                            //device?.alerts
                            device?.id = d
                            device?.let { user.devices.add(it) }
                            devicesNames = user.devices.map { it.name }.toTypedArray() as Array<String>// List of names
                            devicesIds = user.devices.map { it.id }.toTypedArray() as Array<String> // List of names
                            //recyclerView.adapter = MyAdapter(devicesNames) { position -> showHome(position) }
                            withContext(Dispatchers.Main) {
                                recyclerView.adapter = MyAdapter(devicesNames) { position -> showHome(user, position) }
                            }
                        }

//                        db.collection("Devices").document(d.id).get().addOnSuccessListener { document->
//                            //println(getDeviceFromDocument(document))
//                            val alertsList: ArrayList<Alert> = arrayListOf()
//                            document.reference.collection("alerts").get()
//                                    .addOnSuccessListener { documents ->
//                                        for (doc in documents){
//                                           alertsList.add(Alert(doc.get("type").toString(), doc.getTimestamp("time")?.toDate()))
//                                        }
//                                        val device = Device(document.id, document.get("name").toString(), document.get("phoneNumber").toString(), alertsList, document.get("contactNumber").toString())
//                                        user.devices.add(device)
//                                        devicesNames = user.devices.map { it.name }.toTypedArray() // List of names
//                                        devicesIds = user.devices.map { it.id }.toTypedArray() // List of names
//                                        //recyclerView.adapter = MyAdapter(devicesNames) { position -> showHome(position) }
//                                        recyclerView.adapter = MyAdapter(devicesNames) { position -> showHome(user, position) }
//                                    }
//                        }
                    }
                }
            } else {
                val data = hashMapOf("email" to email)
                db.collection("Users").document(email).set(data).addOnFailureListener { e -> println("ERROR: $e") }
            }
//            myDataSet= user.devices.map { it.name}.toTypedArray() // List of Ids
//            recyclerView.adapter = MyAdapter(myDataSet) { position -> showHome(position) }
        }
                .addOnFailureListener {
                    println("ERROR")
                }

    }

    override fun onResume() {
        super.onResume()
        //getUserData(email)
    }

}


class MyAdapter(private val mDataSet: Array<String>, private val listener: (Int) -> Unit) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    // En este ejemplo cada elemento consta solo de un nombre
    class ViewHolder(var linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout) {

//        init {
//            textView.setOnClickListener {
//                val homeIntent = Intent(it.context, HomeActivity::class.java).apply {
//                    putExtra("email", email)
//                }
//                it.context.startActivity(homeIntent)
//            }
//        }
    }

    // El layout manager invoca este método para renderizar cada elemento
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_text_view2, parent, false) as LinearLayout

        // Aquí podemos definir tamaños, márgenes, paddings, etc
        return ViewHolder(v)
    }

    // Este método asigna valores para cada elemento de la lista
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.linearLayout.tvName.text = mDataSet[position]
        holder.linearLayout.setOnClickListener {
            listener(position)
        }
    }

    // Cantidad de elementos del RecyclerView
    // Puede ser más complejo (por ejm, si implementamos filtros o búsquedas)
    override fun getItemCount() = mDataSet.size

}

