package com.lpro4.androidapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_alerts.*
import java.util.*
import kotlin.collections.ArrayList

class AlertsActivity : BaseActivity() {
    lateinit var device: Device
    lateinit var adapter: AlertAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_alerts)

        val bundle: Bundle? = intent.extras
        var alertsList: ArrayList<Alert> = bundle?.get("alerts") as ArrayList<Alert>
        device = bundle.get("device") as Device
        val deviceId: String? = bundle.getString("device-id")


        if (alertsList.isEmpty()) {
            tvNoAlerts.visibility = View.VISIBLE
            rvAlerts.visibility = View.GONE
        } else {
            tvNoAlerts.visibility = View.GONE
            rvAlerts.visibility = View.VISIBLE
        }

        title = getString(R.string.alerts)

        adapter = AlertAdapter(alertsList, device, deviceId!!)
        rvAlerts.adapter = adapter
        rvAlerts.layoutManager = LinearLayoutManager(this)

        swipeLayout.setOnRefreshListener {
            refresh()
        }

        swipeLayout.isRefreshing = true
        refresh()

    }

    private fun refresh() {
        val db = Firebase.firestore
        db.collection("Devices").document(device.id!!).get().addOnSuccessListener {
            device = it.toObject()!!
            device.alerts.sort()
            adapter.refreshDataset(device.alerts)
            swipeLayout.isRefreshing = false
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_with_refresh, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.item_refresh -> {
                swipeLayout.post {
                    swipeLayout.isRefreshing = true
                    refresh()
                }
            }

        }
        return super.onOptionsItemSelected(item)
    }

}