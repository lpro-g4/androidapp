package com.lpro4.androidapp

import android.app.AlertDialog
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.alert_item.view.*
import java.text.SimpleDateFormat


class AlertAdapter(
    private var alertsList: MutableList<Alert>,
    private var device: Device,
    private var deviceId: String
) : RecyclerView.Adapter<AlertAdapter.AlertViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlertViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.alert_item, parent, false)
        return AlertViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AlertViewHolder, position: Int) {
        val currentItem = alertsList[position]
        holder.titleView.text = currentItem.type

        if (DateUtils.isToday(currentItem.time!!.time)) {
            val simpleDateFormat = SimpleDateFormat("HH:mm")
            holder.timeView.text = simpleDateFormat.format(currentItem.time)
        } else {
            val simpleDateFormat = SimpleDateFormat("d MMM. HH:mm")
            holder.timeView.text = simpleDateFormat.format(currentItem.time)
        }
    }

    override fun getItemCount() = alertsList.size

    fun refreshDataset(data: MutableList<Alert>) {
        alertsList = data
        notifyDataSetChanged()
    }

    inner class AlertViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleView: TextView = itemView.tvTitle
        val timeView: TextView = itemView.tvTime
        val imgViewDelete: ImageView = itemView.ivDelete
        val imgView: ImageView = itemView.ivLocation


        init {
            imgView.setOnClickListener {
                val dialog = MapDialog(itemView.context, alertsList[adapterPosition])
                dialog.show()
            }
            imgViewDelete.setOnClickListener {
                val builder = AlertDialog.Builder(it.context)
                builder.setMessage("Are you sure you want to Delete?")
                    .setCancelable(false)
                    .setPositiveButton("Yes") { dialog, id ->
                        // Delete selected note from database
                        device.alerts.remove(alertsList[adapterPosition])
                        Firebase.firestore.collection("Devices").document(deviceId).set(device)
                            .addOnSuccessListener {
                                alertsList.remove(alertsList[adapterPosition])
                                notifyItemRemoved(adapterPosition)
                                notifyItemRangeChanged(adapterPosition, alertsList.size)
                            }
                    }
                    .setNegativeButton("No") { dialog, id ->
                        dialog.dismiss()
                    }
                val alert = builder.create()
                alert.show()
            }
        }

    }
}