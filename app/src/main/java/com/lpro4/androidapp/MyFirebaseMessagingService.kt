package com.lpro4.androidapp

import android.content.Context
import android.util.Log
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import timber.log.Timber

class MyFirebaseMessagingService : FirebaseMessagingService() {


    companion object {
        fun updateToken(token: String, email: String) {

            val db = Firebase.firestore
            val user = hashMapOf(
                    "deviceToken" to token,
            )

            db.collection("Users").document(email)
                    .set(user, SetOptions.merge())
                    .addOnSuccessListener {
                        Timber.d("DocumentSnapshot successfully written!")
                    }
                    .addOnFailureListener { e -> Timber.tag("Settings").w(e, "Error writing document") }
        }
    }

    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Timber.d("Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        val email = getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).getString("email", null) ?: ""
        updateToken(token,email)
    }
}