package com.lpro4.androidapp

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.area_item.view.*
import kotlinx.android.synthetic.main.area_item2.view.*
import java.util.*

class AreaAdapter(
    private var areasList: MutableList<Area>,
    private var device: Device,
    private var context: BaseActivity,
    private var deviceId: String
) : ArrayAdapter<Area>(context,R.layout.area_item,areasList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

//        if(convertView == null){
//            convertView = LayoutInflater.from(parent.context).inflate(R.layout.area_item, parent, false)
//        }

        val view = LayoutInflater.from(parent.context).inflate(R.layout.area_item2, parent, false)
        view.tvArea.text = areasList[position].name
        view.ivDelete.setOnClickListener {
            val builder = AlertDialog.Builder(it.context)
            builder.setMessage("Are you sure you want to Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id ->
                    // Delete selected note from database
                    device.areas.remove(areasList[position])
                    Firebase.firestore.collection("Devices").document(deviceId).set(device)
                        .addOnSuccessListener {
                            areasList.remove(areasList[position])
                        }
                }
                .setNegativeButton("No") { dialog, id ->
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }
        return view

        //return super.getView(position, convertView, parent)
    }

    override fun getItem(position: Int): Area? {
        return areasList[position]
    }

    override fun getCount(): Int {
        return areasList.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
               val results = FilterResults()
                return results
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
               clear()
            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                return (resultValue as Area).name.toString()
            }

        }
    }



    inner class AreaViewHolder(itemView: View)  {
        var titleView: TextView = itemView.tvArea
        val imgViewDelete: ImageView = itemView.ivDelete


        init {

//            imgViewDelete.setOnClickListener {
//                val builder = AlertDialog.Builder(it.context)
//                builder.setMessage("Are you sure you want to Delete?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes") { dialog, id ->
//                        // Delete selected note from database
//                        device.areas.remove(areasList[adapterPosition])
//                        Firebase.firestore.collection("Devices").document(deviceId).set(device)
//                            .addOnSuccessListener {
//                                areasList.remove(areasList[adapterPosition])
////                                notifyItemRemoved(adapterPosition)
////                                notifyItemRangeChanged(adapterPosition, areasList.size)
//                            }
//                    }
//                    .setNegativeButton("No") { dialog, id ->
//                        dialog.dismiss()
//                    }
//                val alert = builder.create()
//                alert.show()
//            }
        }

    }

}