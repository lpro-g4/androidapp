package com.lpro4.androidapp

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.common.PlaceConstants
import com.mapbox.mapboxsdk.style.layers.CircleLayer
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_area.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.area_name_dialog.view.*
import java.util.*


class AreaActivity : BaseActivity() {

    private lateinit var mapView: MapView
    private lateinit var area: Polygon
    private lateinit var mapBoxMap: MapboxMap

    private var fillLayerPointList: MutableList<Point> = ArrayList()
    private var lineLayerPointList: MutableList<Point> = ArrayList()
    private var circleLayerFeatureList: MutableList<Feature> = ArrayList()

    private val circleSourceId = "circle-source-id"
    private val fillSourceId = "fill-source-id"
    private val lineSourceId = "line-source-id"
    private val circleLayerId = "circle-layer-id"
    private val fillLayerId = "fill-layer-polygon-id"
    private val lineLayerId = "line-layer-id"

    private var circleSource: GeoJsonSource? = null
    private var fillSource: GeoJsonSource? = null
    private var lineSource: GeoJsonSource? = null
    private var firstPointOfPolygon: Point? = null
    private var listOfList: ArrayList<List<Point>>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.draw_area)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // MapBox

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

        setContentView(R.layout.activity_area)

        checkButton.visibility = View.INVISIBLE
        clearButton.visibility = View.INVISIBLE


        mapView = findViewById(R.id.mapView2)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            mapBoxMap = mapboxMap

            mapboxMap.setStyle(Style.MAPBOX_STREETS) { style ->

                mapboxMap.addOnMapClickListener { point ->


                    val mapTargetPoint = Point.fromLngLat(point.longitude, point.latitude)

                    // Make note of the first map click location so that it can be used to create a closed polygon later on
                    // Make note of the first map click location so that it can be used to create a closed polygon later on
                    if (circleLayerFeatureList.size === 0) {
                        firstPointOfPolygon = mapTargetPoint
                    }

                    // Add the click point to the circle layer and update the display of the circle layer data
                    circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint))
                    circleSource?.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList))

                    // Add the click point to the line layer and update the display of the line layer data

                    // Add the click point to the line layer and update the display of the line layer data
                    if (circleLayerFeatureList.size < 3) {
                        lineLayerPointList.add(mapTargetPoint)
                    } else if (circleLayerFeatureList.size === 3) {
                        lineLayerPointList.add(mapTargetPoint)
                        firstPointOfPolygon?.let { lineLayerPointList.add(it) }
                    } else {
                        lineLayerPointList.removeAt(circleLayerFeatureList.size - 1)
                        lineLayerPointList.add(mapTargetPoint)
                        firstPointOfPolygon?.let { lineLayerPointList.add(it) }
                    }
                    lineSource?.setGeoJson(FeatureCollection.fromFeatures(arrayOf(Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList)))))

                    // Add the click point to the fill layer and update the display of the fill layer data

                    // Add the click point to the fill layer and update the display of the fill layer data
                    if (circleLayerFeatureList.size < 3) {
                        fillLayerPointList.add(mapTargetPoint)
                        checkButton.visibility = View.INVISIBLE
                        clearButton.visibility = View.INVISIBLE;

                    } else if (circleLayerFeatureList.size === 3) {
                        fillLayerPointList.add(mapTargetPoint)
                        firstPointOfPolygon?.let { fillLayerPointList.add(it) }
                        checkButton.visibility = View.VISIBLE
                        clearButton.visibility = View.VISIBLE

                    } else {
                        fillLayerPointList.removeAt(fillLayerPointList.size - 1)
                        fillLayerPointList.add(mapTargetPoint)
                        firstPointOfPolygon?.let { fillLayerPointList.add(it) }
                        checkButton.visibility = View.VISIBLE
                        clearButton.visibility = View.VISIBLE


                    }
                    listOfList = ArrayList()
                    listOfList!!.add(fillLayerPointList)
                    val finalFeatureList: MutableList<Feature> = ArrayList()
                    finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList!!)))
                    val newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList)
                    fillSource?.setGeoJson(newFeatureCollection)

                    return@addOnMapClickListener true
                }

                // Add sources to the map
                circleSource = initCircleSource(style)
                fillSource = initFillSource(style)
                lineSource = initLineSource(style)

                // Add layers to the map
                initCircleLayer(style)
                initLineLayer(style)
                initFillLayer(style)

                Toast.makeText(this, "Toca en el mapa para añadir puntos", Toast.LENGTH_LONG).show()

            }

            checkButton.setOnClickListener {
                placeSelected()
            }

            clearButton.setOnClickListener {
                clearEntireMap()
            }

        }

    }


    /**
     * Set up the CircleLayer source for showing map click points
     */
    private fun initCircleSource(loadedMapStyle: Style): GeoJsonSource {
        val circleFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val circleGeoJsonSource = GeoJsonSource(circleSourceId, circleFeatureCollection)
        loadedMapStyle.addSource(circleGeoJsonSource)
        return circleGeoJsonSource
    }

    /**
     * Set up the CircleLayer for showing polygon click points
     */
    private fun initCircleLayer(loadedMapStyle: Style) {
        val circleLayer = CircleLayer(circleLayerId,
                circleSourceId)
        circleLayer.setProperties(
                circleRadius(7f),
                circleColor(Color.parseColor("#d004d3"))
        )
        loadedMapStyle.addLayer(circleLayer)
    }

    /**
     * Set up the FillLayer source for showing map click points
     */
    private fun initFillSource(loadedMapStyle: Style): GeoJsonSource {
        val fillFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val fillGeoJsonSource = GeoJsonSource(fillSourceId, fillFeatureCollection)
        loadedMapStyle.addSource(fillGeoJsonSource)
        return fillGeoJsonSource
    }

    /**
     * Set up the FillLayer for showing the set boundaries' polygons
     */
    private fun initFillLayer(loadedMapStyle: Style) {
        val fillLayer = FillLayer(fillLayerId,
                fillSourceId)
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#00e9ff"))
        )
        loadedMapStyle.addLayerBelow(fillLayer, lineLayerId)
    }

    /**
     * Set up the LineLayer source for showing map click points
     */
    private fun initLineSource(loadedMapStyle: Style): GeoJsonSource {
        val lineFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        val lineGeoJsonSource = GeoJsonSource(lineSourceId, lineFeatureCollection)
        loadedMapStyle.addSource(lineGeoJsonSource)
        return lineGeoJsonSource
    }

    /**
     * Set up the LineLayer for showing the set boundaries' polygons
     */
    private fun initLineLayer(loadedMapStyle: Style) {
        val lineLayer = LineLayer(lineLayerId,
                lineSourceId)
        lineLayer.setProperties(
                lineColor(Color.WHITE),
                lineWidth(5f)
        )
        loadedMapStyle.addLayerBelow(lineLayer, circleLayerId)
    }

    /**
     * Remove the drawn area from the map by resetting the FeatureCollections used by the layers' sources
     */
    private fun clearEntireMap() {
        fillLayerPointList = ArrayList()
        circleLayerFeatureList = ArrayList()
        lineLayerPointList = ArrayList()
        if (circleSource != null) {
            circleSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        }
        if (lineSource != null) {
            lineSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        }
        if (fillSource != null) {
            fillSource!!.setGeoJson(FeatureCollection.fromFeatures(arrayOf()))
        }

        checkButton.visibility = View.INVISIBLE
        clearButton.visibility = View.INVISIBLE
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun placeSelected() {


        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.save_area))

        val dialogView = this.layoutInflater.inflate(R.layout.area_name_dialog, null)
        builder.setView(dialogView)
        builder.apply {
            setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                val returningIntent = Intent()
                area = listOfList?.let { Polygon.fromLngLats(it.toMutableList()) }!!
                returningIntent.putExtra("area", area)
                returningIntent.putExtra(PlaceConstants.MAP_CAMERA_POSITION, mapBoxMap.cameraPosition)
                returningIntent.putExtra("points", fillLayerPointList.toTypedArray())
                returningIntent.putExtra("area-name", dialogView.editTextName.text.toString())
                setResult(RESULT_OK, returningIntent)
                finish()
            })
            setNegativeButton("Calcel", DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
        }

        val dialog = builder.create()
        dialog.setOnShowListener(DialogInterface.OnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        })
        dialogView.editTextName.addTextChangedListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = it?.length!! > 0
        }
        dialog.show()
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

}
