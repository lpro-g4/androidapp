package com.lpro4.androidapp

import android.app.Application
import android.content.Context
import java.util.*

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        var change = ""
        val sharedPreferences = getSharedPreferences(getString(R.string.prefs_file),Context.MODE_PRIVATE)
        val language = sharedPreferences.getString("language", "english")
        if (language == "spanish") {
            change="es"
        } else if (language=="english" ) {
            change = "en"
        }else {
            change =""
        }

        BaseActivity.dLocale = Locale(change) //set any locale you want here
        val prefs = getSharedPreferences(getString(R.string.prefs_file),Context.MODE_PRIVATE).edit()
        prefs.putString("language", language)
        prefs.apply()
    }
}