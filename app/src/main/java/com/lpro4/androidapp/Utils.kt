package com.lpro4.androidapp

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import android.os.Parcel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

fun getDeviceFromDocument(document: DocumentSnapshot): Device? {

    val dev: Device? = document.toObject<Device>()
    document.reference.collection("alerts").get().addOnSuccessListener {
        dev?.alerts = it.toObjects<Alert>() as ArrayList<Alert>
    }
    return dev
}


//fun getDeviceById(id: String): Device? {

//    val tutorialDoc = Firebase.firestore.collection("alerts").document("mecagoendios")
//    val device: Device? = Firebase.firestore.collection("devices").document(id).get().await().toObject<Device>()
//    val alerts = Firebase.firestore.collection("devices").document(id).collection("alerts").get().await().toObjects<Alert>()
//    device?.alerts = alerts as ArrayList<Alert>
//    return device
//}

fun readGeoPointArrayList(parcel: Parcel): ArrayList<GeoPoint>? {
    var N: Int = parcel.readInt()
    if (N < 0) {
        return null
    }
    val l: java.util.ArrayList<GeoPoint> = arrayListOf()
    //readListInternal(l, N, loader)
    while (N > 0) {
        val value: GeoPoint = GeoPoint(parcel.readDouble(), parcel.readDouble())
        //Log.d(TAG, "Unmarshalling value=" + value);
        //Log.d(TAG, "Unmarshalling value=" + value);
        l.add(value)
        N--
    }
    return l
}

fun writeGeoPointList(`val`: List<GeoPoint>?, parcel: Parcel) {
    if (`val` == null) {
        parcel.writeInt(-1)
        return
    }
    val N = `val`.size
    var i = 0
    parcel.writeInt(N)
    while (i < N) {
        parcel.writeDouble(`val`[i].latitude)
        parcel.writeDouble(`val`[i].longitude)
        i++
    }
}

fun polygonFromPoints(points: List<Point>): Polygon {
    val listOfList: ArrayList<List<Point>> = arrayListOf()
    listOfList.add(points)
    return Polygon.fromLngLats(listOfList)
}

fun geoPointsToPoints(geoPoints: ArrayList<GeoPoint>):List<Point>{
    val points: MutableList<Point> = mutableListOf()
    for(gp in geoPoints)
        points.add(Point.fromLngLat(gp.longitude,gp.latitude))
    return points
}

fun polygonFromGeoPoints(geoPoints: ArrayList<GeoPoint>): Polygon {
    val listOfList: ArrayList<List<Point>> = arrayListOf()
    listOfList.add(geoPointsToPoints(geoPoints))
    return Polygon.fromLngLats(listOfList)
}

fun pointsToGeoPoints(points: Array<Point>):ArrayList<GeoPoint>{
    val geoPoints: ArrayList<GeoPoint> = arrayListOf()
    for (p in points){
        geoPoints.add(GeoPoint(p.latitude(),p.longitude()))
    }
    return geoPoints
}

class ContextUtils(base: Context) : ContextWrapper(base) {

    companion object {

        fun updateLocale(c: Context, localeToSwitchTo: Locale): ContextWrapper {
            var context = c
            val resources: Resources = context.resources
            val configuration: Configuration = resources.configuration
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val localeList = LocaleList(localeToSwitchTo)
                LocaleList.setDefault(localeList)
                configuration.setLocales(localeList)
            } else {
                configuration.locale = localeToSwitchTo
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                context = context.createConfigurationContext(configuration)
            } else {
                resources.updateConfiguration(configuration, resources.displayMetrics)
            }
            return ContextUtils(context)
        }
    }
}