package com.lpro4.androidapp

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import kotlinx.android.synthetic.main.map_dialog.*

class MapDialog(context: Context, private val alert: Alert) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE)
        super.onCreate(savedInstanceState)
        setTitle("asd")


        Mapbox.getInstance(context, "pk.eyJ1IjoibHBybzQyMDIxIiwiYSI6ImNrbHkxMTd4ajE1d2QycHBsaXZ3M2FpeXUifQ.sEZWbH6nifNb_-TqjXDXNw")
        setContentView(R.layout.map_dialog)
        val mapView: MapView = mapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->

            mapboxMap.setStyle(Style.MAPBOX_STREETS) {

// Map is set up and the style has loaded. Now you can add data or make other map adjustments

                val symbolManager = SymbolManager(mapView, mapboxMap, it)
                symbolManager.iconAllowOverlap = true
                symbolManager.iconIgnorePlacement = true;
                it.addImage("myMarker", BitmapFactory.decodeResource(context.resources, R.drawable.map_default_map_marker))


                val target: LatLng = LatLng(alert.location?.latitude!!, alert.location.longitude)
                symbolManager.create(SymbolOptions()
                        .withLatLng(target)
                        .withIconImage("myMarker")
                )
                val cameraPosition = CameraPosition.Builder()
                        .target(target)
                        .zoom(14.0)
                        .build()
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 100)
            }

        }
        btnOk.setOnClickListener {
            this.dismiss()
        }
    }
}