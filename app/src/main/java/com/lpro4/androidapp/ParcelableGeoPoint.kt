package com.lpro4.androidapp

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue


data class ParcelableGeoPoint(var latitude: Double? = 0.0, var longitude: Double? = 0.0)


