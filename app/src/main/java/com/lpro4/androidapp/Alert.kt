package com.lpro4.androidapp

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import java.util.*


data class Alert(val type: String? = "",
                 val time: Date? = null,
                 val location: GeoPoint? = null) : Parcelable, Comparable<Alert> {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            time = Date(parcel.readLong()),
            GeoPoint(parcel.readDouble(), parcel.readDouble())

    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeDate(time)
        location?.latitude?.let { parcel.writeDouble(it) }
        location?.longitude?.let { parcel.writeDouble(it) }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Alert> {
        override fun createFromParcel(parcel: Parcel): Alert {
            return Alert(parcel)
        }

        override fun newArray(size: Int): Array<Alert?> {
            return arrayOfNulls(size)
        }
    }

    fun Parcel.writeDate(date: Date?) {
        writeLong(date?.time ?: -1)
    }

    fun Parcel.readDate(): Date? {
        val long = readLong()
        return if (long != -1L) Date(long) else null
    }

    override fun compareTo(other: Alert): Int {
        return -time!!.compareTo(other.time)
    }



}